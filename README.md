# ABOUT

I made a C++ version of this game on 12/10/2022 while I was in the 4th semester of Computer Science. It was a console-dependent program with very low execution performance. Now, I'm creating a new version in Rust.

# GAME SERIES

[X] Asteroids
[ ] Breakout
[ ] Tetris
[ ] Snake
[ ] Pong
[ ] Maze
[ ] Space Invaders


