use ggez::{
    event, glam::Vec2, graphics::{self, Color, Rect, Text}, input::keyboard::{KeyCode, KeyInput}, Context, GameResult
};
use rand::{distributions::Uniform, prelude::Distribution, rngs::ThreadRng, Rng};
use std::collections::VecDeque;

const SCREEN_WIDTH: f32 = 640.0;
const SCREEN_HEIGHT: f32 = 480.0;
const SNAKE_SIZE: f32 = 30.0;
const FRUIT_SIZE: f32 = 30.0;

#[derive(PartialEq)]
pub enum GameStatus { PLAYING, DEAD }

#[derive(Clone, Copy, PartialEq)]
enum Direction {STOP, W, D, S, A}

pub struct SnakeBody {
    x: f32,
    y: f32,
}

impl SnakeBody {
    fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }
}

pub struct SnakeHead {
    x: f32,
    y: f32,
}

pub struct Snake {
    head: SnakeHead,
    body: Vec<SnakeBody>,
    direction: Direction,
}

impl Snake {
    fn new() -> Self {
        let head = SnakeHead {
            x: SCREEN_WIDTH / 2.0,
            y: SCREEN_HEIGHT / 2.0,
        };
        Self {
            head,
            body: Vec::new(),
            direction: Direction::STOP,
        }
    }

    fn draw(&self, canvas: &mut graphics::Canvas) {
        canvas.draw(
            &graphics::Quad,
            graphics::DrawParam::new()
                .dest_rect(Rect::new(
                    self.head.x,
                    self.head.y,
                    SNAKE_SIZE,
                    SNAKE_SIZE,
                ))
                .color([0.0, 0.8, 0.0, 1.0]),
        );

        for segment in &self.body {
            canvas.draw(
                &graphics::Quad,
                graphics::DrawParam::new()
                    .dest_rect(Rect::new(
                        segment.x,
                        segment.y,
                        SNAKE_SIZE,
                        SNAKE_SIZE,
                    ))
                    .color([0.0, 0.8, 0.0, 1.0]),
            );
        }
    }
}

struct Food {
    x: f32,
    y: f32,
}

impl Food {
    fn new() -> Self {
        Self {
            x: SNAKE_SIZE + rand::thread_rng()
                .gen_range(0.0..(SCREEN_WIDTH - SNAKE_SIZE)),
            y: SNAKE_SIZE + rand::thread_rng()
                .gen_range(0.0..(SCREEN_HEIGHT - SNAKE_SIZE)),
        }
    }

    fn draw(&self, canvas: &mut graphics::Canvas) {
        canvas.draw(
            &graphics::Quad,
            graphics::DrawParam::new()
                .dest_rect(Rect::new(
                    self.x,
                    self.y,
                    FRUIT_SIZE,
                    FRUIT_SIZE,
                ))
                .color([0.8, 0.0, 0.0, 1.0]),
        );
    }
}

struct GameState {
    snake: Snake,
    food: Food,
    cur_score: u32,
    top_score: u32,
}

impl GameState {
    pub fn new() -> Self {
        Self {
            snake: Snake::new(),
            food: Food::new(),
            cur_score: 0,
            top_score: GameState::read_score(),
        }
    }

    fn read_score() -> u32 {
        0
    }

    fn write_score(&self) {}

    fn restart(&mut self) {
        self.snake = Snake::new();
        self.food = Food::new();
        self.cur_score = 0;
    }
}

impl event::EventHandler<ggez::GameError> for GameState {
    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        let mut canvas = graphics::Canvas::from_frame(
            ctx,
            graphics::Color::from([0.08, 0.08, 0.08, 1.0]),
        );
        self.snake.draw(&mut canvas);
        self.food.draw(&mut canvas);
        
        canvas.finish(ctx)?;
        ggez::timer::yield_now();
        Ok(())
    }

    fn update(&mut self, ctx: &mut Context) -> GameResult {
        if ctx.time.check_update_time(8) {
            // Atualiza a posição da cobra com base na direção
            let (head_x, head_y) = (self.snake.head.x, self.snake.head.y);

            match self.snake.direction {
                Direction::W => self.snake.head.y -= SNAKE_SIZE,
                Direction::D => self.snake.head.x += SNAKE_SIZE,
                Direction::S => self.snake.head.y += SNAKE_SIZE,
                Direction::A => self.snake.head.x -= SNAKE_SIZE,
                _ => (),
            }

            // Inverter a posição ao atingir os limites
            if self.snake.head.y < 0.0 {
                self.snake.head.y = SCREEN_HEIGHT - SNAKE_SIZE;
            } else if self.snake.head.y >= SCREEN_HEIGHT {
                self.snake.head.y = 0.0;
            }

            if self.snake.head.x < 0.0 {
                self.snake.head.x = SCREEN_WIDTH - SNAKE_SIZE;
            } else if self.snake.head.x >= SCREEN_WIDTH {
                self.snake.head.x = 0.0;
            }

            // Verifica se a cabeça colidiu com o corpo
            for body in &self.snake.body {
                if self.snake.head.x == body.x && self.snake.head.y == body.y {
                    if self.cur_score > self.top_score {
                        self.top_score = self.cur_score;
                        self.write_score();
                    }
                    self.restart();
                    return Ok(());
                }
            }

            // Checa se comeu a fruta
            let fruit_rect = Rect::new(self.food.x, self.food.y, FRUIT_SIZE, FRUIT_SIZE);
            let head_rect = Rect::new(self.snake.head.x, self.snake.head.y, SNAKE_SIZE, SNAKE_SIZE);

            if head_rect.overlaps(&fruit_rect) {
                let new_body = SnakeBody::new(self.snake.body.last().map_or(self.snake.head.x, |b| b.x), self.snake.body.last().map_or(self.snake.head.y, |b| b.y));
                self.snake.body.push(new_body);
                self.cur_score += 10;

                // Respawn food
                self.food = Food::new();
            }

            // Atualiza o corpo da cobra
            if !self.snake.body.is_empty() {
                for i in (1..self.snake.body.len()).rev() {
                    self.snake.body[i].x = self.snake.body[i - 1].x;
                    self.snake.body[i].y = self.snake.body[i - 1].y;
                }
                self.snake.body[0].x = head_x;
                self.snake.body[0].y = head_y;
            }

            return Ok(());
        }

        Ok(())
    }

    fn key_down_event(&mut self, _ctx: &mut Context, input: KeyInput, _repeat: bool) -> GameResult {
        if let Some(e) = input.keycode {
            match e {
                KeyCode::Up    if self.snake.direction != Direction::S => self.snake.direction = Direction::W,
                KeyCode::Right if self.snake.direction != Direction::A => self.snake.direction = Direction::D,
                KeyCode::Down  if self.snake.direction != Direction::W => self.snake.direction = Direction::S,
                KeyCode::Left  if self.snake.direction != Direction::D => self.snake.direction = Direction::A,
                _ => ()
            }
        }

        Ok(())
    }
}

fn main() -> GameResult {
    let (ctx, events_loop) = ggez::ContextBuilder::new("snake", "Gray Olson")
        .window_setup(ggez::conf::WindowSetup::default().title("snake"))
        .window_mode(ggez::conf::WindowMode::default().dimensions(SCREEN_WIDTH, SCREEN_HEIGHT))
        .build()?;

    let state = GameState::new();

    event::run(ctx, events_loop, state)
}

